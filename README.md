# Vagrant Docker
This vagrant project serves as a boilerplate to get up and running with docker inside vagrant (without using the native provisioner).

Since there are issues with the network setup on vagrant, the secondary network interface is not automatically configured with vagrant but it's manually configured in the provision shell script. The assigned ip to the VM is `192.168.6.8`.
